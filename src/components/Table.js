import React, { useMemo } from 'react'
import { useTable, useGlobalFilter, useFilters} from 'react-table'
import Data from './movies.json'
import { COLUMNS } from './columns'
import './table.css'
import { GlobalFilter } from './GlobalFilter'
import { ColumnFilter } from './ColumnFilter'
import { TopDiffusion } from './TopDiffusion'
import { TopPrime } from './TopPrime'

export const Table = () => {

    const columns = useMemo(() => COLUMNS, [])
    const data = useMemo(() => Data, [])

    const defaultColumn = useMemo(() => {
        return {
            Filter: ColumnFilter
        }
    }, [])

    const tableInstance = useTable({
        columns,
        data,
    })

    const { getTableProps, 
            getTableBodyProps, 
            headerGroups, 
            footerGroups, 
            rows, 
            prepareRow, 
            state, 
            setGlobalFilter, } = useTable ({ columns, data, defaultColumn}, useFilters, useGlobalFilter,)

    const { globalFilter } = state

    return(
        <>
        <h1>Les Films les plus diffusés des années 2010</h1>
        <TopDiffusion /> &emsp; <TopPrime />
        <br/><br/>
        <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter}/>
        <br/><br/>
        <table {...getTableProps()}>
            <thead>
                {
                    headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {
                                headerGroup.headers.map( column => (
                                    <th {...column.getHeaderProps()}>{column.render('Header')}
                                        <div>{column.canFilter ?  column.render('Filter') : null}</div>
                                    </th>
                                ))}
                        </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {
                    rows.map(row => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {
                                    row.cells.map( cell => {
                                        return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                    })
                                }
                            </tr>
                        )
                    })
                }
                
            </tbody>
            <tfoot>
                {
                    footerGroups.map(footerGroup =>(
                        <tr {...footerGroup.getFooterGroupProps()}>
                            {
                                footerGroup.headers.map(column => (
                                    <td {...column.getFooterProps}>
                                        {
                                            column.render('Footer')
                                        }
                                    </td>
                                ))
                            }
                        </tr>
                    ))
                }
            </tfoot>
        </table>
        </>
    )
}
