import { ColumnFilter } from "./ColumnFilter"

export const COLUMNS = [
    {
        Header: 'Nom du Film',
        Footer: 'Nom du Film',
        accessor: 'nom',
        //Filter: ColumnFilter,
        //disableFilters: true

    },
    {
        Header: 'Réalisateur',
        Footer: 'Réalisateur',
        accessor: 'realisateur',
        //Filter: ColumnFilter
    },
    {
        Header: 'Année de Production',
        Footer: 'Année de Production',
        accessor: 'annee_production',
        //Filter: ColumnFilter
    },
    {
        Header: 'Nationalité',
        Footer: 'Nationalité',
        accessor: 'nationalite',
        //Filter: ColumnFilter
    },
    {
        Header: 'Dernière Diffusion',
        Footer: 'Dernière Diffusion',
        accessor: 'derniere_diffusion',
        //Filter: ColumnFilter
    }
]